package control;

import entities.Forest;
import graphics.Screen;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;

import javax.swing.JFrame;

public class Controller extends Canvas implements Runnable {

	private static final long serialVersionUID = 1L;
	private static int width = 100;
	private static int height = 100;
	private static int scale = 8;
	public static String title = "Forest Ecology";

	public Thread thread;
	public JFrame frame;

	private boolean running = false;

	private Screen screen;
	private Forest forest;

	private BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
	private int[] pixels = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();

	public Controller() {
		Dimension size = new Dimension(width * scale, height * scale);
		setPreferredSize(size);

		screen = new Screen(width, height, scale);
		frame = new JFrame();
		forest = Forest.getInstance(width);

	}

	public synchronized void start() {
		running = true;
		thread = new Thread(this, "Display");
		thread.start();
	}

	public synchronized void stop() {
		running = false;
		try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		long lastTime = System.nanoTime();
		long timer = System.currentTimeMillis();
		final double ns = 1000000000.0 / 200.0;
		double delta = 0;

		requestFocus();
		while (running) {
			long now = System.nanoTime();
			delta += (now - lastTime) / ns;
			lastTime = now;
			while (delta >= 1) {
				update();
				render();
				delta--;
			}

			if (System.currentTimeMillis() - timer > 1) {
				timer += 1;
				frame.setTitle(title + "  |  Year: " + forest.getYear());
			}
			if (forest.isDead()) {
				this.stop();
			}
			if (forest.getYear() == 400) {
				System.out.println("400 years passed. The simulation will stop now");
				this.stop();
			}
		}
	}

	public void update() {
		forest.monthPassed();
	}

	public void render() {
		BufferStrategy bs = getBufferStrategy();
		if (bs == null) {
			createBufferStrategy(3);
			return;
		}

		screen.clear();
		screen.renderGrid(Forest.grid);

		for (int i = 0; i < pixels.length; i++) {
			pixels[i] = screen.pixels[i];
		}

		Graphics g = bs.getDrawGraphics();
		g.drawImage(image, 0, 0, getWidth(), getHeight(), null);
		g.setColor(Color.WHITE);
		g.setFont(new Font("Verdana", 0, 50));
		g.dispose();
		bs.show();
	}

	public static void main(String[] args) {
		Controller game = new Controller();
		game.frame.setResizable(false);
		game.frame.setTitle(Controller.title);
		game.frame.add(game);
		game.frame.pack();
		game.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		game.frame.setLocationRelativeTo(null);
		game.frame.setVisible(true);
		game.start();
	}
}
