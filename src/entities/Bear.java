package entities;

public class Bear extends Entity{
	private static int gridColor = 0x61210B;
	
	public Bear(int x, int y) {
		super(x, y);
	}

	public int getGridColor(){
		return gridColor;
	}
}
