package entities;

public abstract class Entity {
	
	private static int gridColor;
	private int x;
	private int y;
	
	public Entity(){
		
	}
	
	public Entity(int x, int y){
		this.x = x;
		this.y = y;
	}

	public int getGridColor() {
		return gridColor;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

}
