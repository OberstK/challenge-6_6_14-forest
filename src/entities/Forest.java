package entities;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;

import util.Helper;
import util.Slot;
import util.SlotGrid;

public class Forest {
	public static SlotGrid grid;
	public static int size;
	private static Forest instance;
	private Helper help;
	private Random rand;
	private List<Bear> bears;
	private List<Tree> trees;
	private List<Sapling> saplings;
	private List<Lumberjack> lumberjacks;
	private int maws;
	private int monthMaws;
	private int monthWood;
	private int monthSaps;
	private int monthTreesGrown;
	private int wood;
	private int month;
	private int year;
	private int newBears;
	private int capturedBears;
	private int yearWood;
	private int newLumberjacks;
	private boolean dead;

	private Forest(int size) {
		help = new Helper();
		this.dead = false;
		Forest.size = size;
		this.year = 0;
		this.month = 0;
		Forest.grid = new SlotGrid(size);
		rand = new Random();
		bears = new ArrayList<Bear>();
		lumberjacks = new ArrayList<Lumberjack>();
		trees = new ArrayList<Tree>();
		saplings = new CopyOnWriteArrayList<Sapling>();
		generateForest(size);
	}

	public static Forest getInstance(int size) {
		if (instance == null) {
			instance = new Forest(size);
		} else {
			if (Forest.size != size) {
				System.out.println("Error! Forest with different size already exists!");
			}
		}
		return instance;
	}

	// Generator
	public void generateForest(int size) {
		for (int i = 0; i < 0.5 * (size * size); i++) {
			spawnRandomTree();
		}
		for (int i = 0; i < 0.1 * (size * size); i++) {
			spawnRandomLumberjack();
		}
		for (int i = 0; i < 0.02 * (size * size); i++) {
			spawnRandomBear();
		}
	}

	// Month-Actions
	public void monthPassed() {
		this.month++;
		printMonthStatus();
		if (month == 12) {
			yearPassed();
			month = 0;
		}
		makeMoves();
		growTrees();
		growSaplings();
		if (plantsLeft() == false) {
			this.dead = true;
		}
	}

	public void makeMoves() {
		for (Lumberjack l : lumberjacks) {
			moveLumberjack(l);
		}
		for (Bear b : bears) {
			moveBear(b);
		}
	}

	// Year-Actions
	public void yearPassed() {
		year++;
		endOfYearCheck();
		printYearStatus();
	}

	public void endOfYearCheck() {
		if (maws != 0) {
			hireZoo();
			capturedBears++;
		} else {
			spawnRandomBear();
			newBears++;
		}
		maws = 0;
		this.yearWood = wood;
		if (lumberjacks.size() <= 1) {
			for (int i = 0; i < wood; i++) {
				spawnRandomLumberjack();
			}
			this.newLumberjacks = wood;
		} else if (Math.round(wood / lumberjacks.size()) == 0) {
			Lumberjack remove = lumberjacks.remove(0);
			grid.getSlot(remove.getX(), remove.getY()).setLumberjack(null);
			this.newLumberjacks = -1;
		} else {
			int newLumbers = Math.round(wood / lumberjacks.size());
			for (int i = 0; i < newLumbers; i++) {
				spawnRandomLumberjack();
			}
			this.newLumberjacks = newLumbers;
		}
		wood = 0;
	}

	// Mover
	public void moveBear(Bear bear) {
		int startX = bear.getX();
		int startY = bear.getY();
		int turn = 0;
		for (int i = 0; i < 5; i++) {
			turn++;
			Slot sNew = help.getMovableSlot(startX, startY);
			Slot sOld = grid.getSlot(startX, startY);

			if (sNew.getLumberjack() != null) {
				bear.setX(sNew.getX());
				bear.setY(sNew.getY());
				sNew.setBear(bear);
				sOld.setBear(null);
				lumberjacks.remove(sNew.getLumberjack());
				sNew.setLumberjack(null);
				maw();
				break;
			} else if (sNew.getBear() != null && turn > 1) {
				break;
			} else if (sNew.getBear() != null) {
				continue;
			} else {
				bear.setX(sNew.getX());
				bear.setY(sNew.getY());
				sNew.setBear(bear);
				sOld.setBear(null);
				startX = sNew.getX();
				startY = sNew.getY();
			}
		}
	}

	public void moveLumberjack(Lumberjack l) {
		int startX = l.getX();
		int startY = l.getY();
		int turn = 0;

		for (int i = 0; i < 3; i++) {
			turn++;
			Slot sNew = help.getMovableSlot(startX, startY);
			Slot sOld = grid.getSlot(startX, startY);
			if (sNew.getLumberjack() != null && turn > 1) {
				break;
			} else if (sNew.getLumberjack() != null) {
				continue;
			} else if (sNew.getBear() != null) {
				grid.getSlot(startX, startY).setLumberjack(null);
				lumberjacks.remove(sNew.getLumberjack());
				maw();
				break;
			} else if (sNew.getTree() != null) {
				l.setX(sNew.getX());
				l.setY(sNew.getY());
				sNew.setLumberjack(l);

				if (sNew.getTree().isElder()) {
					wood = wood + 2;
					monthWood = monthWood + 2;
				} else {
					wood++;
					monthWood++;
				}
				trees.remove(sNew.getTree());
				sNew.setTree(null);
				sOld.setLumberjack(null);
				break;
			} else {
				l.setX(sNew.getX());
				l.setY(sNew.getY());
				sNew.setLumberjack(l);
				sOld.setLumberjack(null);
			}
			startX = sNew.getX();
			startY = sNew.getY();
		}
	}

	// Actions
	private void maw() {
		maws++;
		monthMaws++;
		if (lumberjacks.isEmpty()) {
			spawnRandomLumberjack();
		}
	}

	public void growTrees() {
		for (Tree t : trees) {
			t.growOlder();

			if (t.getAge() == 120) {
				monthTreesGrown++;
				t.setElder();
			}
			if (t.isElder()) {
				if (rand.nextInt(10) == 0) {
					spawnSapling(t);
				}
			} else {
				if (rand.nextInt(20) == 0) {
					spawnSapling(t);
				}
			}
		}
	}

	public void growSaplings() {
		for (Sapling sap : saplings) {
			sap.growOlder();
			if (sap.getAge() == 12) {
				monthSaps++;
				int x = sap.getX();
				int y = sap.getY();
				Tree t = new Tree(x, y);
				Slot s = grid.getSlot(x, y);
				saplings.remove(s.getSapling());
				s.setSap(null);
				trees.add(t);
				s.setTree(t);
			}
		}
	}

	public void hireZoo() {
		int bearNumber = rand.nextInt(bears.size());
		Bear b = bears.remove(bearNumber);
		grid.getSlot(b.getX(), b.getY()).setBear(null);
	}

	// Spawner
	public void spawnSapling(Tree t) {
		Map<Integer, Slot> emptySlots = help.getEmptySlotsAroundSlot(t.getX(), t.getY());

		if (emptySlots.size() == 0) {
			return;
		}

		int randomSpot = rand.nextInt(emptySlots.size());
		Slot s = emptySlots.get(randomSpot);
		Sapling sap = new Sapling(s.getX(), s.getY());
		s.setSap(sap);
		saplings.add(sap);
	}

	public void spawnRandomTree() {
		int x = 0;
		int y = 0;
		do {
			x = rand.nextInt(size);
			y = rand.nextInt(size);
		} while (grid.getSlot(x, y).isEmpty() == false);

		Tree t = new Tree(x, y);
		grid.getSlot(x, y).setTree(t);
		trees.add(t);
	}

	public void spawnRandomBear() {
		int x = 0;
		int y = 0;
		do {
			x = rand.nextInt(size);
			y = rand.nextInt(size);
		} while (grid.getSlot(x, y).getLumberjack() != null);

		Bear b = new Bear(x, y);
		grid.getSlot(x, y).setBear(b);
		bears.add(b);
	}

	public void spawnRandomLumberjack() {
		int x = 0;
		int y = 0;
		do {
			x = rand.nextInt(size);
			y = rand.nextInt(size);
		} while (grid.getSlot(x, y).getBear() != null);
		Lumberjack l = new Lumberjack(x, y);
		grid.getSlot(x, y).setLumberjack(l);
		lumberjacks.add(l);
	}

	// Printer
	public void printForest() {
		for (int i = 0; i < grid.getSize(); i++) {
			for (int j = 0; j < grid.getSize(); j++) {
				Entity ent = grid.getSlot(i, j).getEntityByPriority();
				if (ent instanceof Tree) {
					System.out.println("T");
				} else if (ent instanceof Lumberjack) {
					System.out.println("L");
				} else if (ent instanceof Bear) {
					System.out.println("B");
				}
			}
		}
	}

	public void printMonthStatus() {
		if (monthMaws != 0) {
			System.out.println("Month [" + String.format("%03d", month) + "]: [" + monthMaws + "] Lumberjacks were maw'd by bears.");
		}
		if (monthSaps != 0) {
			System.out.println("Month [" + String.format("%03d", month) + "]: [" + monthSaps + "] new Saplings created.");
		}
		if (monthTreesGrown != 0) {
			System.out.println("Month [" + String.format("%03d", month) + "]: [" + monthTreesGrown + "] Trees became Elder Treess.");
		}
		if (monthWood != 0) {
			System.out.println("Month [" + String.format("%03d", month) + "]: [" + monthWood + "] pieces of lumber harvested by Lumberjacks.");
		}
		monthMaws = 0;
		monthSaps = 0;
		monthTreesGrown = 0;
		monthWood = 0;
	}

	public void printYearStatus() {
		int elder = 0;
		for (Tree t : trees) {
			if (t.isElder()) {
				elder++;
			}
		}
		System.out.println("Year [" + String.format("%03d", year) + "]: Forest has: " + bears.size() + " Bears, " + lumberjacks.size() + " Lumberjacks, " + trees.size() + " Trees, " + saplings.size() + " Saplings and " + elder + " Elder Trees.");
		if (newBears != 0) {
			System.out.println("Year [" + String.format("%03d", year) + "]: 1 new Bear added.");
		} else if (capturedBears != 0) {
			System.out.println("Year [" + String.format("%03d", year) + "]: 1 Bear captured by Zoo.");
		}
		if (newLumberjacks != -1) {
			System.out.println("Year [" + String.format("%03d", year) + "]: " + yearWood + " wood has been harvested " + newLumberjacks + " new Lumberjacks hired.");
		} else {
			System.out.println("Year [" + String.format("%03d", year) + "]: " + yearWood + " wood has been harvested 1 Lumberjack was fired.");
		}
		newBears = 0;
		capturedBears = 0;
		newLumberjacks = 0;
		yearWood = 0;
	}

	// Getter
	public int getMonth() {
		return month;
	}

	public int getYear() {
		return year;
	}

	public boolean isDead() {
		return dead;
	}

	// End of Game Check

	public boolean plantsLeft() {
		if (saplings.size() == 0 && trees.size() == 0) {
			System.out.println("This forest is dead! No Saplings or Trees are left. You killed this poor forest. Feel ashamed for it!");
			return false;
		}
		return true;
	}
}
