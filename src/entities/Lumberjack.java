package entities;

public class Lumberjack extends Entity{
	private static int gridColor = 0xFE2E2E;
	
	public Lumberjack(int x, int y){
		super(x,y);
	}
	public int getGridColor(){
		return gridColor;
	}
}
