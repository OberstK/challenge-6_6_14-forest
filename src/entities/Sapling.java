package entities;

public class Sapling extends Entity {
	private static int gridColor = 0x2EFE2E;
	private int age;

	public Sapling() {
		this.age = 0;
	}

	public Sapling(int x, int y) {
		super(x, y);
		this.age = 0;
	}

	public int getGridColor() {
		return gridColor;
	}

	public void growOlder() {
		age++;
	}

	public int getAge() {
		return age;
	}
}
