package entities;

public class Tree extends Entity {
	private boolean elder;
	private int age;
	private static int gridColorElder = 0x0B610B;
	private static int gridColorNormal = 0x04B404;

	public Tree(int x, int y) {
		super(x, y);
		this.elder = false;
	}

	public int getGridColor() {
		if (elder) {
			return gridColorElder;
		}
		return gridColorNormal;
	}

	public void setElder() {
		elder = true;
	}

	public boolean isElder() {
		return elder;
	}

	public void growOlder() {
		age++;
	}

	public int getAge() {
		return age;
	}
}
