package graphics;

import entities.Entity;
import util.Slot;
import util.SlotGrid;

public class Screen {

	public int width, height, scale;
	public int[] pixels;

	public Screen(int width, int height, int scale) {
		this.width = width;
		this.height = height;
		this.scale = scale;
		pixels = new int[width * height];
	}

	public void clear() {
		for (int i = 0; i < pixels.length; i++) {
			pixels[i] = 0;
		}
	}

	public void renderGrid(SlotGrid eGrid) {
		Slot[] grid = eGrid.getGrid();
		for (int i = 0; i < grid.length; i++) {
			Entity ent = grid[i].getEntityByPriority();
			if (ent != null) {
				pixels[i] = ent.getGridColor();
			} else {
				pixels[i] = 0xCEF6CE;
			}
		}
	}
}
