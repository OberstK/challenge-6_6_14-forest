package util;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import entities.Forest;

public class Helper {
	private Random rand;

	public Helper() {
		rand = new Random();
	}

	public int generateRandomOffset() {
		return rand.nextInt(3) - 1;
	}

	public Map<Integer, Slot> getEmptySlotsAroundSlot(int x, int y) {
		int topborder = 0;
		int botborder = 0;
		int leftborder = 0;
		int rightborder = 0;

		if (y - 1 >= 0) {
			topborder = y - 1;
		} else {
			topborder = y;
		}

		if (y + 1 < Forest.size) {
			botborder = y + 1;
		} else {
			botborder = y;
		}

		if (x - 1 >= 0) {
			leftborder = x - 1;
		} else {
			leftborder = x;
		}

		if (x + 1 < Forest.size) {
			rightborder = x + 1;
		} else {
			rightborder = x;
		}

		Map<Integer, Slot> emptySlots = new HashMap<Integer, Slot>();
		int key = 0;
		for (int i = leftborder; i <= rightborder; i++) {
			for (int j = topborder; j <= botborder; j++) {
				Slot s = Forest.grid.getSlot(i, j);
				if (s.isEmpty()) {
					emptySlots.put(key, s);
					key++;
				}
			}
		}
		return emptySlots;
	}

	public Slot getMovableSlot(int x, int y) {
		int newX = 0;
		int newY = 0;
		int changeX = 0;
		int changeY = 0;
		do {
			changeX = generateRandomOffset();
			changeY = generateRandomOffset();
		} while (changeX == 0 && changeY == 0);

		newX = x + changeX;
		newY = y + changeY;
		if (newX < 0) {
			newX = Forest.size - 1;
		}
		if (newX == Forest.size) {
			newX = 0;
		}
		if (newY < 0) {
			newY = Forest.size - 1;
		}
		if (newY == Forest.size) {
			newY = 0;
		}
		return Forest.grid.getSlot(newX, newY);
	}

}
