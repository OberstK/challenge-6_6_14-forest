package util;

import entities.Bear;
import entities.Entity;
import entities.Lumberjack;
import entities.Sapling;
import entities.Tree;

public class Slot {
	private int x;
	private int y;
	private Bear b;
	private Lumberjack l;
	private Tree t;
	private Sapling s;

	public Slot(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public Entity getEntityByPriority() {
		if (b != null) {
			return b;
		} else if (l != null) {
			return l;
		} else if (t != null) {
			return t;
		} else if (s != null) {
			return s;
		} else {
			return null;
		}
	}

	public Bear getBear() {
		return b;
	}

	public void setBear(Bear bear) {
		this.b = bear;
	}

	public Lumberjack getLumberjack() {
		return l;
	}

	public void setLumberjack(Lumberjack l) {
		this.l = l;
	}

	public Tree getTree() {
		return t;
	}

	public void setTree(Tree t) {
		this.t = t;
	}

	public Sapling getSapling() {
		return s;
	}

	public void setSap(Sapling sap) {
		this.s = sap;
	}

	public boolean isEmpty() {
		if (s == null && t == null && l == null && b == null) {
			return true;
		}
		return false;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
}
