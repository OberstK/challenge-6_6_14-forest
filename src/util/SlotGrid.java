package util;

public class SlotGrid {
	private Slot[] grid;
	private int size;

	public SlotGrid(int size) {
		this.size = size;
		grid = new Slot[size * size];
		generateSlots();
	}

	public void generateSlots() {
		int x = 0;
		int y = 0;
		for (int i = 0; i < size * size; i++) {
			if (i % size == 0 && i != 0) {
				y++;
				x = 0;
			}
			grid[i] = new Slot(x, y);
			x++;
		}
	}

	public Slot getSlot(int x, int y) {
		return grid[x + y * size];
	}

	public Slot[] getGrid() {
		return grid;
	}

	public int getSize() {
		return size;
	}
}
